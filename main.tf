
# Configure the AWS Provider
provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

## don't hard-code credentiels directly in the config file for security perspective
## config files will be hosted in a version control system

resource "aws_vpc" "dev-aws_vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "dev-sub-1" {
  vpc_id            = aws_vpc.dev-aws_vpc.id
  cidr_block        = "10.0.10.0/24"
  availability_zone = "us-east-1a"
}

data "aws_vpc" "existing_vpc" {
  default = true
}

resource "aws_subnet" "dev-sub-2" {
  vpc_id            = data.aws_vpc.existing_vpc.id
  cidr_block        = "172.31.10.0/24" ## doit specifier selon le cidr block du vpc et les subnets existants
  availability_zone = "us-east-1a"
}

output "vpc_id" {
  value = aws_vpc.dev-aws_vpc.id
}
